package com.Gmail.test.GmailUITest;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailLoginTest {
	
	WebDriver driver;
	
	@Before
	public void invokeBrowser(){
		System.setProperty("webdriver.chrome.driver", "D:/Selenium/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		System.out.println("1111111111111111");
		driver.manage().window().maximize();
		System.out.println("22222222222222222");
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.get("http://gmail.com");
	}
	
	@Test
	public void login(){
		WebElement usrNm = driver.findElement(By.xpath("//*[@id='identifierId']"));
		usrNm.sendKeys("stestashr");
		driver.findElement(By.xpath("//*[@id='identifierId']")).sendKeys(Keys.ENTER);
		WebElement pwd = driver.findElement(By.xpath("//*[@id='password']/div[1]/div/div[1]/input"));
		pwd.sendKeys("Password@07");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id='password']/div[1]/div/div[1]/input")).sendKeys(Keys.ENTER);
		String url = driver.getCurrentUrl();
		System.out.println("URL ::"+url);
		WebDriverWait wait= new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.partialLinkText("Inbox"))));
//		Assert.assertTrue("Invox shoudl exist", driver.findElements(By.partialLinkText("Inbox")).size() >0);
		System.out.println("Success");
	}
	
	@After
	public void closeBrowser(){
		driver.quit();
	}

}
